#!/bin/bash -ex

# CONFIG
prefix="AIM"
suffix=""
munki_package_name="AIM"
display_name="AIM"

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' http://download.newaol.com/aim/mac/AIM_Install.dmg

## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

# Obtain Version Number from InfoPlist.strings
sed -n 5p "${mountpoint}/AIM.app/Contents/Resources/en.lproj/InfoPlist.strings" > Test.plist
version=`cut -c 62-74 Test.plist`
rm -rf Test.plist

/usr/local/munki/makepkginfo -f "${mountpoint}/AIM.app/Contents/Resources/en.lproj/InfoPlist.strings" > md5checksum.plist

hdiutil detach "${mountpoint}"

# Build pkginfo
/usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg > app.plist

PLISTBUDDY="/usr/libexec/PlistBuddy"

#Create Second Component of Installs Array
${PLISTBUDDY} -c "Add :installs:1:md5checksum string" app.plist 
${PLISTBUDDY} -c "Add :installs:1:path string" app.plist
${PLISTBUDDY} -c "Add :installs:1:type string" app.plist

#Set Values for Second Component of Installs Array
AIM_md5checksum=`${PLISTBUDDY} -c "Print :installs:0:md5checksum" md5checksum.plist`
AIM_path="/Applications/AIM.app/Contents/Resources/en.lproj/InfoPlist.strings"
AIM_type="file"

rm -f md5checksum.plist

#Finalize Installs Array
${PLISTBUDDY} -c "Set :installs:1:md5checksum ${AIM_md5checksum}" app.plist
${PLISTBUDDY} -c "Set :installs:1:path ${AIM_path}" app.plist
${PLISTBUDDY} -c "Set :installs:1:type ${AIM_type}" app.plist

plist=`pwd`/app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.7.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
